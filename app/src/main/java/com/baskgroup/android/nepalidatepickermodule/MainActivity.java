package com.baskgroup.android.nepalidatepickermodule;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.baskgroup.android.nepalidatepicker.CalendarView.Calendar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button=(Button)findViewById(R.id.btn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImageDialog(view);
            }
        });
    }
        public void showImageDialog(final View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
        ViewGroup viewGroup = view.findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(view.getContext()).inflate(R.layout.activity_calender, viewGroup, false);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        Calendar calendar =(Calendar)dialogView.findViewById(R.id.calendar);
        calendar.setOnDateSetListener(new Calendar.OnDateSetListener() {
            @Override
            public void onDateClick(View calendar, int year, int month, int day) {
                Toast.makeText(getApplicationContext(),""+year,Toast.LENGTH_SHORT).show();
            }
        });
        alertDialog.show();
    }
}